import "./App.css";
import Comp from "./component"; // Make sure the component is capitalized
import store from "./redux/store";
import { Provider } from "react-redux";

function App() {
  return (
    <Provider store={store}>
      <Comp />
    </Provider>
  );
}

export default App;
