import { emptyUser, fetchUsers } from "./redux/actions";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";

function Comp() {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users);

  const checkdata = () => {
    if (!users || users.length === 0) {
      dispatch(fetchUsers());
    } else {
      dispatch(emptyUser());
    }
  };

  return (
    <div className="boss">
      {users.map((user) => (
        <div className="body" key={user.id}>
          <div className="name">{user.name}</div>
          <div className="email">{user.email}</div>
          <hr style={{ width: "100%" }} />
        </div>
      ))}
      <button className="fetch" onClick={checkdata}>
        fetch
      </button>
    </div>
  );
}

export default Comp;
