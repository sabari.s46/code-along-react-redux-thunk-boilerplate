export const FETCH_USERS_START = "FETCH_USERS_START";
export const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";
export const FETCH_USERS_FAILURE = "FETCH_USERS_FAILURE";

export const EMPTY_USER = "EMPTY_USER";

export const emptyUser = () => ({
  type: EMPTY_USER,
});
export const fetchUsersStart = () => ({
  type: FETCH_USERS_START,
});

export const fetchUsersSuccess = (users) => ({
  type: FETCH_USERS_SUCCESS,
  payload: users,
});

export const fetchUsersFailure = (error) => ({
  type: FETCH_USERS_FAILURE,
  payload: error,
});

export const fetchUsers = () => {
  return (dispatch) => {
    dispatch(fetchUsersStart());
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        if (!res.ok) {
          throw new Error(`Error fetching users. Status: ${res.status}`);
        }
        return res.json();
      })
      .then((data) => dispatch(fetchUsersSuccess(data)))
      .catch((error) => {
        dispatch(fetchUsersFailure(error));
      });
  };
};
